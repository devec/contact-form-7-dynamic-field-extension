<?php

/*
Plugin Name: Contact Form 7 - Dynamic Field Extension
Plugin URI: http://todo.org
Description: Provides a dynamic text field that accepts any shortcode to generate the content.  Requires Contact Form 7 . This is a fork of Contact Form 7 - Dynamic Text Extension .
Version: 2.0.2.1
Author: Alexander Gerber
Author URI: http://todo.org
License: GPL2
*/

/*  Copyright 2010-2017  Alexander Gerber

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License, version 2, as 
    published by the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

defined( 'ABSPATH' ) or die( 'No script kiddies please!' );


require_once plugin_dir_path( __FILE__ ) . '/ContactFormShortcodes.php';
require_once plugin_dir_path( __FILE__ ) . '/TagGenerator.php';
require_once plugin_dir_path( __FILE__ ) . '/DynamicTextShortcodes.php';


class ContactForm7DynamicFieldExtension {
	function __construct() {
		 $contactFormShortcodes = new ContactFormShortcodes();
		 $tagGenerator = new TagGenerator();
		 $dynamicTextShortcodes = new DynamicTextShortcodes();
	}
}
if(!isset($contactForm7DynamicFieldExtension)){
	$contactForm7DynamicFieldExtension = new ContactForm7DynamicFieldExtension();
}

