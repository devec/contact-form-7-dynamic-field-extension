<?php

require_once plugin_dir_path( __FILE__ ) . '/Output.php';

/**
 * @return string
 */
function shortcodeHandlerText( $tag ) {
	
	$attsHandler = attsHandler( $tag );
	$tag = $attsHandler->tag;
	$atts = $attsHandler->atts;
	$value = $attsHandler->value;
	$validation_error = $attsHandler->validation_error;
	
	$atts['value'] = $value;
	
	switch( $tag->basetype ){
		case 'dynamictext':
			$atts['type'] = 'text';
			break;
		case 'dynamichidden':
			$atts['type'] = 'hidden';
			break;
		default:
			$atts['type'] = 'text';
			break;
	}

	$atts['name'] = $tag->name;

	$atts['size'] = $tag->get_size_option( '40' );
	$atts = wpcf7_format_atts( $atts );

	$html = sprintf(
			'<span class="wpcf7-form-control-wrap %1$s"><input %2$s />%3$s</span>',
			sanitize_html_class( $tag->name ), $atts, $validation_error );

	return $html;
}

/**
  * @return string
  */
function shortcodeHandlerTextarea( $tag ) {
	
	$attsHandler = attsHandler( $tag );
	$tag = $attsHandler->tag;
	$atts = $attsHandler->atts;
	$value = $attsHandler->value;
	$validation_error = $attsHandler->validation_error;

	$atts['type'] = 'textarea';
	$atts['name'] = $tag->name;
	$atts['cols'] = $tag->get_cols_option( '40' );
	$atts['rows'] = $tag->get_rows_option( '10' );
	
	$atts = wpcf7_format_atts( $atts );

	$html = sprintf(
		'<span class="wpcf7-form-control-wrap %1$s"><textarea %2$s>%3$s</textarea>%4$s</span>',
		sanitize_html_class( $tag->name ), $atts, $value, $validation_error );

	return $html;
}

/**
 * @return object
 */
function attsHandler( $tag ) {
	$tag = new WPCF7_FormTag( $tag );

	if ( empty( $tag->name ) )
		return '';

	$outputAtts = setAtts( $atts, $tag );
	$atts = $outputAtts->atts;
	$validation_error = $outputAtts->validation_error;

	$outputValue = setValue( $atts, $tag );
	$atts = $outputValue->atts;
	$value = $outputValue->value;

	
	$out = new Output();
	$out->tag = $tag;
	$out->atts = $atts;
	$out->value = $value;
	$out->validation_error = $validation_error;

	return $out;
}

/**
 * @return object
 */
function setValue( $atts, $tag ) {
	$value = (string) reset( $tag->values );

	if ( $tag->has_option( 'placeholder' ) || $tag->has_option( 'watermark' ) ) {
		$atts['placeholder'] = $value;
		$value = '';
	}

	$value = $tag->get_default_option( $value );

	$value = wpcf7_get_hangover( $tag->name, $value );

	$scval = do_shortcode('['.$value.']');
	if( $scval != '['.$value.']' ){
		$value = esc_attr( $scval );
	}

	$out = new Output();

	$out->atts = $atts;
	$out->value = $value;

	return $out;
}

/**
 * @return object
 */
function setAtts( $atts, $tag ) {

	$validation_error = wpcf7_get_validation_error( $tag->name );

	$class = wpcf7_form_controls_class( $tag->type, 'wpcf7dtx-dynamictext' );


	if ( $validation_error )
		$class .= ' wpcf7-not-valid';

	$atts = checkLength($atts, $tag);


	$atts['class'] = $tag->get_class_option( $class );
	$atts['id'] = $tag->get_id_option();
	$atts['tabindex'] = $tag->get_option( 'tabindex', 'int', true );

	if ( $tag->has_option( 'readonly' ) )
		$atts['readonly'] = 'readonly';

	if ( $tag->is_required() )
		$atts['aria-required'] = 'true';

	$atts['aria-invalid'] = $validation_error ? 'true' : 'false';

	$out = new Output();

	$out->atts = $atts;
	$out->validation_error = $validation_error;

	return $out;
}

/**
 * @return array
 */
function checkLength( $atts, $tag ) {
	
	$atts['maxlength'] = $tag->get_maxlength_option();
	$atts['minlength'] = $tag->get_minlength_option();

	if ( $atts['maxlength'] && $atts['minlength'] && $atts['maxlength'] < $atts['minlength'] ) {
		unset( $atts['maxlength'], $atts['minlength'] );
	}

	return $atts;

}