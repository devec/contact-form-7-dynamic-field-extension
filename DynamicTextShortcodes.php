<?php

require_once plugin_dir_path( __FILE__ ) . '/ShortcodeHandler.php';


class DynamicTextShortcodes {
	function __construct() {
		add_action( 'plugins_loaded', array( $this, 'pluginInit') , 20 );
    }

    /**
     * @return void
     */
    function pluginInit(){
		add_action( 'wpcf7_init', array( $this, 'addShortcode') );
		add_filter( 'wpcf7_validate_dynamictext*', array( $this, 'validationFilter'), 10, 2 );
    }

    /**
     * @return void
     */
	function addShortcode() {
		
		wpcf7_add_form_tag(
			array( 'dynamictext' , 'dynamictext*' , 'dynamichidden' ),
			'shortcodeHandlerText' , true );
		
		wpcf7_add_form_tag(
			array( 'dynamictextarea', 'dynamictextarea*' ),
			'shortcodeHandlerTextarea' , true );
    }

	/**
      * @return string
      */
	  function validationFilterCheckValue( $result, $tag ) {
        if ( ! empty( $value ) ) {
			$maxlength = $tag->get_maxlength_option();
			$minlength = $tag->get_minlength_option();
	
			if ( $maxlength && $minlength && $maxlength < $minlength ) {
				$maxlength = $minlength = null;
			}
	
			$code_units = wpcf7_count_code_units( $value );
	
			if ( false !== $code_units ) {
				if ( $maxlength && $maxlength < $code_units ) {
					$result->invalidate( $tag, wpcf7_get_message( 'invalid_too_long' ) );
				} elseif ( $minlength && $code_units < $minlength ) {
					$result->invalidate( $tag, wpcf7_get_message( 'invalid_too_short' ) );
				}
			}
        }
        return $result;
    }
    /**
     * @return object
     */
	function validationFilter( $result, $tag ) {
		$tag = new WPCF7_FormTag( $tag );
	
		$name = $tag->name;
	
		$value = isset( $_POST[$name] )
			? trim( wp_unslash( strtr( (string) $_POST[$name], "\n", " " ) ) )
			: '';
	
		if ( 'dynamictext' == $tag->basetype ) {
			if ( $tag->is_required() && '' == $value ) {
				$result->invalidate( $tag, wpcf7_get_message( 'invalid_required' ) );
			}
		}
		return validationFilterCheckValue( $result, $tag );
	}
}