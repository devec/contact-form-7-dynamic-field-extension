<?php
/*****************************************************
 * CF7 DTX Included Shortcodes
 * 
 * Used like this:
 * 
 * CF7_GET val='value'
 * 
 * No [] and single quotes ' rather than double "
 * 
 *****************************************************/
class ContactFormShortcodes {
	function __construct() {
        add_shortcode('CF7_GET', array( $this, 'getVar'));
        add_shortcode('CF7_bloginfo', array( $this, 'bloginfo'));
        add_shortcode('CF7_POST', array( $this, 'postVar'));
        add_shortcode('CF7_get_post_var', array( $this, 'getPostVar'));
        add_shortcode('CF7_URL', array( $this, 'currentUrl'));
        add_shortcode('CF7_get_custom_field', array( $this, 'getCustomField'));
        add_shortcode('CF7_get_current_user', array( $this, 'getCurrentUser'));
        add_shortcode( 'CF7_referrer' , array( $this, 'getReferrer'));
        add_filter('wpcf7_cf7com_links', array( $this, 'comLinks'));
    }

    /**
     * @return string
     */
    function getVar($atts){
	    extract(shortcode_atts(array(
		'key' => 0,
	    ), $atts));
	    $value = '';
	    if( isset( $_GET[$key] ) ){
		    $value = urldecode($_GET[$key]);
	    }
	    return $value;
    }

    /* See http://codex.wordpress.org/Function_Reference/get_bloginfo */

    /**
     * @return string
     */
    function bloginfo($atts){
	    extract(shortcode_atts(array(
		    'show' => 'name'
	    ), $atts));
	
	    return get_bloginfo($show);
    }

    /* Insert a $_POST variable (submitted form value)*/

    /**
     * @return string
     */
    function postVar($atts){
	    extract(shortcode_atts(array(
	    	'key' => -1,
	    ), $atts));
	    if($key == -1) return '';
	    $val = '';
	    if( isset( $_POST[$key] ) ){
		    $val = $_POST[$key];
	    }
	    return $val;
    }

    /* Insert a $post (Blog Post) Variable */

    /**
     * @return string
     */
    function getPostVar($atts){
	    extract(shortcode_atts(array(
	    	'key' => 'post_title',
    	), $atts));
	
	    switch($key){
    		case 'slug':
			    $key = 'post_name';
			    break;
		    case 'title':
			    $key = 'post_title';
		    	break;
	    }   
	
	    global $post;
	    $val = $post->$key;
	    return $val;
    }

    /**
     * @return string
     */
    function currentUrl(){
        $pageURL = 'http';
        if( isset( $_SERVER["HTTPS"] ) && $_SERVER["HTTPS"] == "on"){ $pageURL .= "s"; }
        
        $pageURL .= "://";
        
        if( isset( $_SERVER["SERVER_PORT"] ) && $_SERVER["SERVER_PORT"] != "80" ){
            $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
        } else {
            $pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
        }
        return $pageURL;
    }
    /**
     * @return string
     */
    function obfuscate($val){
        $link = '';
        foreach(str_split($val) as $letter)
            $link .= '&#'.ord($letter).';';
        return $link;
    }

    /**
     * @return string
     */
    function getCustomField($atts){
        extract(shortcode_atts(array(
            'key' => '',
            'post_id' => -1,
            'obfuscate'	=> 'off'
        ), $atts));
        
        if($post_id < 0){
            global $post;
            if(isset($post)) $post_id = $post->ID;
        }
        
        if($post_id < 0 || empty($key)) return '';
            
        $val = get_post_meta($post_id, $key, true);
        
        if($obfuscate == 'on'){
            $val = $this->obfuscate($val);
        }
        
        return $val;
        
    }

    /* Insert information about the current user
    * New in 1.0.4 
    * See https://codex.wordpress.org/Function_Reference/wp_get_current_user 
    */

    /**
     * @return string
     */
    function getCurrentUser($atts){
        extract(shortcode_atts(array(
            'key' => 'user_login',
        ), $atts));
        $val = '';
        if( is_user_logged_in() ) {
            $current_user = wp_get_current_user();
            $val = $current_user->$key;
        }
        return $val;
    }

    /**
     * @return boolean
     */
    function getReferrer( $atts ){
        return isset( $_SERVER['HTTP_REFERER'] ) ? $_SERVER['HTTP_REFERER'] : '';
    }

    /**
     * @return string
     */
    function comLinks() {
        $links = '<div class="cf7com-links">'
            . '<a href="' . esc_url_raw( __( 'http://contactform7.com/', 'wpcf7' ) ) . '" target="_blank">'
            . esc_html( __( 'Contactform7.com', 'wpcf7' ) ) . '</a>&ensp;'
            . '<a href="' . esc_url_raw( __( 'http://contactform7.com/docs/', 'wpcf7' ) ) . '" target="_blank">'
            . esc_html( __( 'Docs', 'wpcf7' ) ) . '</a> - '
            . '<a href="' . esc_url_raw( __( 'http://contactform7.com/faq/', 'wpcf7' ) ) . '" target="_blank">'
            . esc_html( __( 'FAQ', 'wpcf7' ) ) . '</a> - '
            . '<a href="' . esc_url_raw( __( 'http://contactform7.com/support/', 'wpcf7' ) ) . '" target="_blank">'
            . esc_html( __( 'Support', 'wpcf7' ) ) . '</a>'
            . ' - <a href="'. esc_url_raw( __( 'http://sevenspark.com/wordpress-plugins/contact-form-7-dynamic-text-extension', 'wpcf7') )
            . '" target="_blank">'.__( 'Dynamic Text Extension' , 'wpcf7').'</a> by <a href="' . esc_url_raw( __( 'http://sevenspark.com', 'wpcf7') ).'" target="_blank">'
            . esc_html( __( 'SevenSpark', 'wpcf7' ) ). '</a> <a href="'.esc_url_raw( __('http://bit.ly/bVogDN')).'" target="_blank">'
            . esc_html( __( '[Donate]')).'</a>'
            . '</div>';
        return $links;
    }
}